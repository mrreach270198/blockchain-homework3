
peer lifecycle chaincode package mycc.tar.gz --path ./chaincode --lang golang --label mycc_1

peer lifecycle chaincode install mycc.tar.gz

peer lifecycle chaincode queryinstalled

export CC_PACKAGE_ID=


peer lifecycle chaincode approveformyorg --channelID workspace --name mycc --version 1.0 --init-required --package-id $CC_PACKAGE_ID --sequence 1 \
    --tls \
	--cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem

peer lifecycle chaincode checkcommitreadiness --channelID workspace --name mycc --version 1.0 --sequence 1 --output json --init-required

peer lifecycle chaincode commit -o orderer1.workspace:7050 --channelID workspace --name mycc --version 1.0 --sequence 1 --init-required --peerAddresses peer1.developers.workspace:7051 \
    --tls \
	--cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem \
    --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/peers/peer1.developers.workspace/tls/ca.crt

peer chaincode invoke -o orderer1.workspace:7050 --isInit -C workspace -n mycc --peerAddresses peer1.developers.workspace:7051 -c '{"Args":["InitLedger",""]}' --waitForEvent \
    --tls \
	--cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/workspace/orderers/orderer1.workspace/msp/tlscacerts/tlsca.workspace-cert.pem \
    --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/developers.workspace/peers/peer1.developers.workspace/tls/ca.crt




